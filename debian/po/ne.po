# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_ne.po to Nepali
# Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>, 2006.
# Shiva Pokharel <shiva@mpp.org.np>, 2006.
# Shyam Krishna Bal <shyam@mpp.org.np>, 2006.
# Shiva Prasad Pokharel <shiva@mpp.org.np>, 2006.
# Shiva Pokharel <shiva@mpp.org.np>, 2007, 2008.
# Shiva Prasad Pokharel <pokharelshiv@gmail.com>, 2007.
# shyam krishna bal <shyamkrishna_bal@yahoo.com>, 2007.
# Nabin Gautam <nabin@mpp.org.np>, 2007.
# Shyam Krishna Bal <balshyam24@yahoo.com>, 2008.
# Shiva Prasad Pokharel <shiva@mpp.org.np>, 2008, 2010, 2011.
# Jeewal Kunwar <jeewalkunwar1@gmail.com>, 2017.
#
# Translations from iso-codes:
#   Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>, 2006.
#   Shiva Prasad Pokharel <shiva@mpp.org.np>, 2006, 2011.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_ne\n"
"Report-Msgid-Bugs-To: partman-hfs@packages.debian.org\n"
"POT-Creation-Date: 2022-03-23 14:39+0100\n"
"PO-Revision-Date: 2019-08-28 18:10+0000\n"
"Last-Translator: leela <53352@protonmail.com>\n"
"Language-Team: Nepali <info@mpp.org.np>\n"
"Language: ne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-hfs.templates:1001 ../partman-hfs.templates:3001
msgid "HFS"
msgstr ""

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-hfs.templates:2001
msgid "Hierarchical File System"
msgstr ""

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-hfs.templates:4001 ../partman-hfs.templates:6001
msgid "HFS+"
msgstr ""

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-hfs.templates:5001
msgid "Hierarchical File System Plus"
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
msgid "Go back to the menu and correct this problem?"
msgstr "मेनुमा फर्केर यो समस्या सुधार्नुहुन्छ?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
#, fuzzy
msgid ""
"You have not configured an HFS partition that is mounted to /boot/grub. This "
"is needed by your machine in order to be able to boot. Please go back and "
"create an HFS partition that is mounted to /boot/grub."
msgstr ""
"तपाईँको बुट विभाजन तपाईँको हार्ड डिस्कको पहिलो प्राथमिक विभाजनमा अवस्थित छैन । बुट "
"गर्नका लागि तपाईँको मेशिनलाई यो चाहिन्छ । कृपया पछाडि जानुहोस् र बुट विभाजनको रुपमा "
"तपाईँको पहिलो प्राथमिक विभाजनलाई प्रयोग गर्नुहोस् ।"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:7001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is. This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"यदि तपाईँ विभाजन मेनुमा फर्केर जानुहुन्न र यो त्रुटिलाई सुधार्नुहुन्छ भने, यो विभाजन जस्ताको "
"तस्तै प्रयोग हुन्छ । यसको मतलव तपाईँ तपाईँको हार्ड डिस्कबाट बुट गर्न सक्षम हुनुहुने छैन ।"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid "Do you want to return to the partitioning menu?"
msgstr "के तपाईँ विभाजन मेनुमा फर्कनु चाहानुहुन्छ ?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid ""
"The partition ${PARTITION} assigned to ${MOUNTPOINT} starts at an offset of "
"${OFFSET} bytes from the minimum alignment for this disk, which may lead to "
"very poor performance."
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-hfs.templates:8001
msgid ""
"Since you are formatting this partition, you should correct this problem now "
"by realigning the partition, as it will be difficult to change later. To do "
"this, go back to the main partitioning menu, delete the partition, and "
"recreate it in the same position with the same settings. This will cause the "
"partition to start at a point best suited for this disk."
msgstr ""
